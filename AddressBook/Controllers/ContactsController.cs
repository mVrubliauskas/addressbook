﻿using AddressBook.Services;
using AddressBook.ViewModels;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AddressBook.Controllers
{
	[Authorize]
	public class ContactsController : Controller
	{
		private readonly IAddressBookService _svc;

		public ContactsController(IAddressBookService svc)
		{
			_svc = svc;
		}

		public ActionResult Create(int personId)
		{
			return View("Manage", new Contact { PersonId = personId });
		}

		public async Task<ActionResult> Edit(int contactId)
		{
			return View("Manage", await _svc.GetContact(contactId));
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Manage(Contact contact)
		{
			if (ModelState.IsValid)
			{
				await _svc.CreateOrUpdate(contact);
				return RedirectToAction("Index", "People");
			}
			return View();
		}

		[HttpPost]
		[Route("{id:int}")]
		public async Task<bool> Delete(int contactId)
		{
			return await _svc.DeleteContact(contactId);
		}

		protected override void Dispose(bool disposing)
		{
			_svc.Dispose();
			base.Dispose(disposing);
		}
	}
}