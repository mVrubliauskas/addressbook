﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AddressBook.ViewModels
{
	public class Person
	{
		public int? Id { get; set; }

		[Display(Name = "Name")]
		[Required]
		public string Name { get; set; }

		[Display(Name = "Lastname")]
		public string LastName { get; set; }

		[Display(Name = "Birthdate")]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
		public DateTime? Birthdate { get; set; }

		public IList<Contact> Contacts { get; set; } 
	}
}