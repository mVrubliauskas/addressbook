﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace AddressBook.Models.DbContext
{
	public abstract class DbContextBase<T> : System.Data.Entity.DbContext where T : System.Data.Entity.DbContext
	{
		static DbContextBase()
		{
			Database.SetInitializer<T>(null);
		}

		protected DbContextBase() : this("name=contacts") { }

		protected DbContextBase(string name)
			: base(name)
		{
			Configuration.LazyLoadingEnabled = false;
			Configuration.ProxyCreationEnabled = false;
			Configuration.UseDatabaseNullSemantics = true;
		}
		
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<IdKeyDiscoveryConvention>();
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
			modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();
			modelBuilder.Conventions.Remove<StoreGeneratedIdentityKeyConvention>();
			modelBuilder.Conventions.Remove<ForeignKeyIndexConvention>();
			modelBuilder.Properties<DateTime>().Configure(p => p.HasColumnType("datetime2"));
		}
	}
}