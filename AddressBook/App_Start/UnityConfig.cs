using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using AddressBook.Controllers;
using AddressBook.Models;
using AddressBook.Models.DbContext;
using AddressBook.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.Unity;
using Unity.Mvc5;

namespace AddressBook
{
	public static class UnityConfig
	{
		public static void RegisterComponents()
		{
			var container = new UnityContainer();

			container.RegisterType<IAddressBookService, Services.AddressBookService>();
			container.RegisterType<ContactsDbContext>();
			container.RegisterType(typeof(IIdentity), new InjectionFactory(c => HttpContext.Current.User.Identity));
			container.RegisterType<AccountController>(new InjectionConstructor());

			DependencyResolver.SetResolver(new UnityDependencyResolver(container));
		}
	}
}