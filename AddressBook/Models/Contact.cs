﻿using System;

namespace AddressBook.Models
{
	public class Contact
	{
		public int Id { get; private set; }
		public int PersonId { get; private set; }

		public string Address { get; private set; }
		public string Email { get; private set; }
		public string PhoneNumber { get; private set; }
		public Person Person { get; private set; }


		private Contact() { }

		public Contact(string address, string email, string phoneNumber, int personId)
		{
			Set(address, email, phoneNumber, personId);
		}

		public void Update(string address, string email, string phoneNumber)
		{
			Set(address, email, phoneNumber);
		}

		private void Set(string address, string email, string phoneNumber, int? personId = null)
		{
			if (string.IsNullOrEmpty(address) && string.IsNullOrEmpty(phoneNumber) && string.IsNullOrEmpty(email))
				throw new ArgumentNullException();
			Address = address;
			Email = email;
			PhoneNumber = phoneNumber;
			if (personId != null)
				PersonId = personId.Value;
		}
	}
}