﻿using System;
using System.Collections.Generic;

namespace AddressBook.Models
{
	public class Person
	{
		public int Id { get; private set; }

		public string Name { get; private set; }

		public string LastName { get; private set; }

		public DateTime? Birthdate { get; private set; }

		public string UserName { get; private set; }

		public ICollection<Contact> Contacts { get; private set; } 

		private Person() { }

		public Person(string name, string lastName, DateTime? birthdate, string userName)
		{
			if (string.IsNullOrEmpty(userName)) throw new ArgumentNullException("userName");
			Set(name, lastName, birthdate, userName);
		}

		public void Update(string name, string lastName, DateTime? birthdate)
		{
			Set(name, lastName, birthdate);
		}

		private void Set(string name, string lastName, DateTime? birthdate, string userName = null)
		{
			if (string.IsNullOrEmpty(name)) throw new ArgumentNullException("name");
			Name = name;
			LastName = lastName;
			Birthdate = birthdate;
			if (!string.IsNullOrEmpty(userName))
				UserName = userName;
		}
	}
}