﻿using AddressBook.Helpers;
using AddressBook.Services;
using AddressBook.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AddressBook.Controllers
{
	[Authorize]
	public class PeopleController : Controller
	{
		private readonly IAddressBookService _svc;

		public PeopleController(IAddressBookService svc)
		{
			_svc = svc;
		}

		// GET: Contacts
		public async Task<ActionResult> Index()
		{
			var result = await _svc.GetFullContactList().ToPaged(1).ToListAsync();
			return View(result);
		}

		public ActionResult Create()
		{
			return View("Manage");
		}

		public async Task<ActionResult> Edit(int personId)
		{
			return View("Manage", await _svc.GetPerson(personId));
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Manage(Person person)
		{
			if (ModelState.IsValid)
			{
				await _svc.CreateOrUpdate(person);
				return RedirectToAction("Index");
			}
			return View();
		}

		[HttpPost]
		[Route("{id:int}")]
		public async Task<bool> Delete(int personId)
		{
			return await _svc.DeletePerson(personId);
		}

		public async Task<ActionResult> GetList(int page, string searchString = null)
		{
			var result = await _svc.GetFullContactList(searchString).ToPaged(page).ToListAsync();
			return result.Any() ? PartialView("_ListInner", result) : null;
		}

		protected override void Dispose(bool disposing)
		{
			_svc.Dispose();
			base.Dispose(disposing);
		}

	}
}