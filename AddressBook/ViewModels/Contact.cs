﻿using System.ComponentModel.DataAnnotations;
using AddressBook.Helpers;

namespace AddressBook.ViewModels
{
	[DataAnnotationExtentions.AtLeastOnePropertyAttribute("Address", "Email", "PhoneNumber", ErrorMessage = "You must supply at least one value")]
	public class Contact
	{
		public int? Id { get; set; }

		public int PersonId { get; set; }

		[Display(Name = "Address")]
		public string Address { get; set; }

		[Display(Name = "Email")]
		[EmailAddress]
		public string Email { get; set; }

		[StringLength(16, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
		[Display(Name = "Phone")]
		[Phone]
		public string PhoneNumber { get; set; }
	}
}