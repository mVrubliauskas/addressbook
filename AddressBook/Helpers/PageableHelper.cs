﻿using System.Linq;

namespace AddressBook.Helpers
{
	public static class PageableHelper
	{
		private const int PageSize = 10;
		public static IQueryable<T> ToPaged<T>(this IQueryable<T> query, int page)
		{
			page -= page <= 0 ? 0 : 1;
			query = query.Skip(PageSize*page).Take(PageSize);
			return query;
		} 
	}
}