﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace AddressBook.Models.DbContext
{
	public class ContactsDbContext : DbContextBase<ContactsDbContext>
	{
		public ContactsDbContext() { }

		public DbSet<Person> People { get; set; }
		public DbSet<Contact> Contacs { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			var personCfg = modelBuilder.Entity<Person>();
			personCfg.ToTable("People", "dbo");
			personCfg.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
			personCfg.HasMany(p => p.Contacts).WithRequired(p => p.Person).HasForeignKey(p => p.PersonId);		

			var contactCfg = modelBuilder.Entity<Contact>();
			contactCfg.ToTable("Contacts", "dbo");
			contactCfg.Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
		}
	}
}