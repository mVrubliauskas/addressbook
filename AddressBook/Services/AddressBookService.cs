﻿using System;
using System.Linq;
using AddressBook.Models;
using AddressBook.Models.DbContext;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Security.Principal;


namespace AddressBook.Services
{

	public interface IAddressBookService : IDisposable
	{
		IQueryable<ViewModels.Person> GetFullContactList(string searchString = null);
		Task CreateOrUpdate(ViewModels.Person person);
		Task CreateOrUpdate(ViewModels.Contact contact);
		Task<bool> DeletePerson(int id);
		Task<bool> DeleteContact(int id);
		Task<ViewModels.Person> GetPerson(int id);
		Task<ViewModels.Contact> GetContact(int id);
	}

	public class AddressBookService : IAddressBookService
	{

		private readonly ContactsDbContext _db;
		private readonly string _username;

		public AddressBookService(ContactsDbContext db, IIdentity user)
		{
			_db = db;
			_username = user.Name;
		}

		private IQueryable<Person> SecuredPeople
		{
			get { return _db.People.Where(p => p.UserName == _username); }
		}

		private IQueryable<Contact> SecuredContacts
		{
			get { return _db.Contacs.Where(p => p.Person.UserName == _username); }

		}

		public IQueryable<ViewModels.Person> GetFullContactList(string searchString)
		{
			var query = SecuredPeople.Include(p => p.Contacts).Select(p => new ViewModels.Person
			{
				Id = p.Id,
				Name = p.Name,
				Birthdate = p.Birthdate,
				LastName = p.LastName,
				Contacts = p.Contacts.Select(c => new ViewModels.Contact
				{
					Id = c.Id,
					Address = c.Address,
					Email = c.Email,
					PhoneNumber = c.PhoneNumber
				}).ToList()
			}).OrderBy(p => p.Name).ThenBy(p => p.LastName).AsNoTracking();

			if (!string.IsNullOrEmpty(searchString))
			{
				query = query.Where(p => p.Name.Contains(searchString) || p.LastName.Contains(searchString));
			}

			return query;
		}

		public async Task CreateOrUpdate(ViewModels.Person person)
		{
			if (person == null) throw new ArgumentNullException("person");
			if (person.Id != null)
			{
				await Update(person);
				return;
			}
			await Create(person);
		}

		private async Task Create(ViewModels.Person person)
		{
			var newPerson = new Person(person.Name, person.LastName, person.Birthdate, _username);
			_db.People.Add(newPerson);
			await _db.SaveChangesAsync();
		}

		private async Task Update(ViewModels.Person person)
		{
			if (person.Id != null)
			{
				var oldPerson = await GetPersonById(person.Id.Value);
				oldPerson.Update(person.Name, person.LastName, person.Birthdate);
				await _db.SaveChangesAsync();
			}
		}

		public async Task CreateOrUpdate(ViewModels.Contact contact)
		{
			if (contact == null) throw new ArgumentNullException("contact");
			if (contact.Id != null)
			{
				await Update(contact);
				return;
			}
			await Create(contact);
		}

		private async Task Create(ViewModels.Contact contact)
		{
			var newContact = new Contact(contact.Address, contact.Email, contact.PhoneNumber, contact.PersonId);
			_db.Contacs.Add(newContact);
			await _db.SaveChangesAsync();
		}

		private async Task Update(ViewModels.Contact contact)
		{
			if (contact.Id != null)
			{
				var oldPerson = await GetContactById(contact.Id.Value);
				oldPerson.Update(contact.Address, contact.Email, contact.PhoneNumber);
				await _db.SaveChangesAsync();
			}
		}

		public async Task<bool> DeletePerson(int id)
		{
			var person = await GetPersonById(id);
			if (person == null) return false;
			_db.People.Remove(person);
			await _db.SaveChangesAsync();
			return true;
		}

		public async Task<bool> DeleteContact(int id)
		{
			var contact = await GetContactById(id);
			if (contact == null) return false;
			_db.Contacs.Remove(contact);
			await _db.SaveChangesAsync();
			return true;
		}

		public async Task<ViewModels.Person> GetPerson(int id)
		{
			var person = await GetPersonById(id);
			return new ViewModels.Person
			{
				Id = person.Id,
				Birthdate = person.Birthdate,
				LastName = person.LastName,
				Name = person.Name
			};
		}

		public async Task<ViewModels.Contact> GetContact(int id)
		{
			var contact = await GetContactById(id);
			return new ViewModels.Contact
			{
				Id = contact.Id,
				Email = contact.Email,
				Address = contact.Address,
				PhoneNumber = contact.PhoneNumber
			};
		}

		private async Task<Person> GetPersonById(int id)
		{
			return await SecuredPeople.FirstOrDefaultAsync(p => p.Id == id);
		}

		private async Task<Contact> GetContactById(int id)
		{
			return await SecuredContacts.FirstOrDefaultAsync(c => c.Id == id);
		}

		public void Dispose()
		{
			_db.Dispose();
		}
	}
}