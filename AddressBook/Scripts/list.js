﻿var inputDelay = (function () {
	var timer = 0;
	return function (callback, ms) {
		clearTimeout(timer);
		timer = setTimeout(callback, ms);
	};
})();

function initList() {
	$(".contactsTable").data("pages", 1);
	showLastPage();
}

function resetList() {
	$(".contactsTable").data("pages", 0);
	$(".contactsTable").data("no-data", false);
	var tableContents = $(".contactsTable tbody");
	tableContents.fadeOut(500);
	tableContents.remove();
	getNewPage();
	showScrollInfo(true);
}

function showLastPage() {
	var page = $(".contactsTable tbody").last();
	page.fadeIn(500);
	hookDeletes(page);
}

function showScrollInfo(show) {
	if (show) {
		$(".scrollInfo").fadeIn();
	} else {
		$(".scrollInfo").fadeOut();
	}
}

var xhrPageRequest;
function getNewPage() {
	if (xhrPageRequest) return;
	var table = $(".contactsTable");
	if (table.data("no-data")) return;
	var currentPages = table.data("pages");
	var searchString = $("#searchContact").val();
	currentPages++;

	var xhrPageRequestSuccess = function (data) {
		if (data) {
			table.append(data);
			table.data("pages", currentPages);
			showLastPage();
		} else {
			showScrollInfo(false);
			table.data("no-data", true);
		}
	}

	var xhrPageRequestAlways = function () {
		xhrPageRequest = undefined;
	}

	var url = table.data("url");
	xhrPageRequest = $.get(url, { page: currentPages, searchString: searchString });
	$.when(xhrPageRequest)
		.done(xhrPageRequestSuccess)
		.always(xhrPageRequestAlways);
}

function hookDeletes(context) {
	var xhrDeleteSuccess = function () {
		resetList();
	}

	$(".delete-contact", context).on("click", function () {
		var url = $(this).data("url");
		var xhrDeleteContact = $.post(url);
		$.when(xhrDeleteContact)
			.done(xhrDeleteSuccess);
	});

	$(".delete-people", context).on("click", function () {
		var url = $(this).data("url");
		var xhrDeletePerson = $.post(url);
		$.when(xhrDeletePerson)
			.done(xhrDeleteSuccess);
	});
}

$(function () {
	$(window).bind('mousewheel', function () {
		if ($(window).scrollTop() + $(window).height() >= $(document).height() - 50) {
			getNewPage();
		}
	});
	$("#searchContact").keyup(function (e) {
		var paramLength = $(this).val().length;
		if (paramLength >= 3 || e.keyCode === 13) {
			inputDelay(function () {
				resetList();
			}, 600);
		}
		if (paramLength === 0) {
			resetList();
		}
	});
	initList();
});